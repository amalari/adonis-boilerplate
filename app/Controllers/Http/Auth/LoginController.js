'use strict'

class LoginController {
  showLoginForm({
    view
  }) {
    return view.render('auth.login')
  }
}

module.exports = LoginController
