'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserHasRolesSchema extends Schema {
  up () {
    this.create('user_has_roles', (table) => {
      table.string('model_type')
      table.integer('role_id').unsigned().references('id').inTable('roles')
      table.bigInteger('user_id').unsigned().references('id').inTable('users')
      table.index(['user_id', 'model_type'])
      table.primary(['model_type', 'user_id', 'role_id'], "user_has_roles_permission_model_type_primary")
    })
  }

  down () {
    this.drop('user_has_roles')
  }
}

module.exports = UserHasRolesSchema
