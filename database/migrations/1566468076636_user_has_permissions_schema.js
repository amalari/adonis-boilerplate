'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserHasPermissionsSchema extends Schema {
  up () {
    this.create('user_has_permissions', (table) => {
      table.string('model_type')
      table.integer('permission_id').unsigned().references('id').inTable('permissions')
      table.bigInteger('user_id').unsigned().references('id').inTable('users')
      table.index(['user_id', 'model_type'])
      table.primary(['model_type', 'user_id', 'permission_id'], "user_has_permissions_permission_model_type_primary")
    })
  }

  down () {
    this.drop('user_has_permissions')
  }
}

module.exports = UserHasPermissionsSchema
