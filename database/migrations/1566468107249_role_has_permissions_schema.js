'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoleHasPermissionsSchema extends Schema {
  up () {
    this.create('role_has_permissions', (table) => {
      table.integer('permission_id').unsigned().references('id').inTable('permissions')
      table.integer('role_id').unsigned().references('id').inTable('roles')
      table.primary(['permission_id', 'role_id'], 'role_has_permissions_permission_role_primary')
    })
  }

  down () {
    this.drop('role_has_permissions')
  }
}

module.exports = RoleHasPermissionsSchema
